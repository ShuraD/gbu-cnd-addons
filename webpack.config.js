const path = require('path');
const VueLoaderPlugin = require('./src/lib/plugin');

module.exports = {
    mode: 'production',
    entry: {
        common_informer: './src/placeholders/common_informer/index.js',
        deal_add: './src/placeholders/deal_add/index.js',
        deal_positions: './src/placeholders/deal_positions/index.js',
        deal_card_tabs: './src/placeholders/deal_card_tabs/index.js',
        deal_card_top: './src/placeholders/deal_card_top/index.js',
        deal_card_status: './src/placeholders/deal_card_status/index.js',
        task_card_status: './src/placeholders/task_card_status/index.js',
        paint_test: './src/placeholders/task_card_status/paint_button_test.js',
        grant: './src/placeholders/grant/index.js',
        grant_procedure: './src/bp/grant_procedures.js',
        custom_notifications: './src/placeholders/common_informer/custom_notifications.js',
        tax_disputes_bp_73: './src/bp/tax_disputes_bp_73.js',
        nio_bp_74: './src/bp/nio_bp_74.js',
        citizen_appeals_bp_86: './src/bp/citizen_appeals_bp_86.js',
        refactoring: './src/refactoring/app.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};
