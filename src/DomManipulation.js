let $ = jQuery || null;

class Container {
    find(selector, context = null, options = null) {
        let element, timer, config, defaults;
        // set default options
        defaults = {
            timeout: 60000,
            scan_frequency: 300
        };
        // define config based on default & specified options
        config = options && Object.assign(defaults, options) || defaults;

        return new Promise(async (resolve, reject) => {
            try {
                timer = setInterval(function () {
                    if (context) {
                        element = $(selector, context);
                    } else {
                        element = $(selector);
                    }
                    if (element.length) {
                        clearInterval(timer);
                        resolve(element);
                    }
                    config.timeout -= config.scan_frequency;
                    // if (config.timeout <= 0) {
                    //     throw new Error('Timeout error');
                    // }
                }, config.scan_frequency);
            } catch (e) {
                reject(e);
            }
        });
    }
    observe(selector, trackedProperty, callback, args = []) {
        let el, vel, trackedStatus;
        setInterval(function () {
            if (typeof selector.jquery === 'undefined') {
                el = $(selector);
            } else {
                el = selector;
            }
            vel = el.last();
            switch (trackedProperty) {
                case 'visibility':
                    if (!trackedStatus && vel && vel.length && vel.is(':visible')) {
                        callback.call(null, vel, 'visible');
                    } else if (trackedStatus && !vel.length) {
                        callback.call(null, vel, 'hidden');
                    }
                    trackedStatus = vel.is(':visible');
                    break;
                case 'value':
                    if (trackedStatus !== el.val()) {
                        if (args.length) {
                            callback.apply(null, [ el ].concat(args));
                        } else {
                            callback.call(null, el);
                        }
                    }
                    trackedStatus = el.val();
                    break;
            }
        }, 300);
    }
    findInputFieldByLabel(selector, context = null, targetType = 'input', markFieldAsRequired = false, alignLabel = false) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            try {
                let label = await self.find(selector, context);
                label = label.last();
                //label.css('white-space', 'nowrap');
                if (alignLabel) {
                    label.css('text-align', alignLabel);
                }
                if (markFieldAsRequired) {
                    label.append(self.getStarElement().clone());
                }
                let labelParent = label.parents('div').first();
                let target = labelParent.find(targetType);
                resolve(target);
            } catch (e) {
                reject(e);
            }
        });
    }
    getStarElement() {
        return $('<span style="color:red"> *</span>');
    }
    getStarLabel() {
        return $('<p style="display:block;color:red;margin: 15px 0 0 0;">* <small>Звездочкой отмечены обязательные поля</small></p>');
    }
    addModalFormCnd(modalId) {
        let $body = $('body');
        let $modalContainer = $body.find('#modal-container-cnd');
        if (!$modalContainer.length) {
            $body.append('<div id="modal-container-cnd"></div>');
            $modalContainer = $body.find('#modal-container-cnd');
        }
        $modalContainer.append(`<div id="${modalId}"></div>`);
    }
    addCustomSelector(selector) {
        let $body = $('body');
        $body.append(`<div id=${selector}></div>`);
    }
}

module.exports = new Container();