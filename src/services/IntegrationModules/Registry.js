import events from 'events';
const eventEmitterIM = new events.EventEmitter();

window.integrationModules = window.integrationModules || [];

export const actions = {
    register: (module) => {
        window.integrationModules.push(module);
        eventEmitterIM.emit('register', window.integrationModules);
    },
    modify: (module) => {
        let index = window.integrationModules.findIndex(function (el, index) {
            return el.name === module.name;
        });
        window.integrationModules[index] = module;
        eventEmitterIM.emit('modify', window.integrationModules);
    }
};

export const registry = {
    getData: () => window.integrationModules,
    subscribe: (callback) => {
        return new Promise((resolve, reject) => {
            try {
                eventEmitterIM.on('register', function (data) {
                    console.log('data', data);
                    callback(data);
                });
            } catch (e) {
                reject(e);
            }
        })
    }
};