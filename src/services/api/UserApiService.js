import api from "../../services/api/index.js";

export default {
    getCoordinatedByMe(id) {
        return api().get(`/addons/user/${id}/coordinator/list/`);
    },
    getEmployeeList() {
        return api().get(`/addons/employee/list/`);
    },
    setEventWatched(data) {
        return api().post(`/addons/custom-events/watched/${data.custom_event_id}/${data.user_id}`, data);
    }
}
