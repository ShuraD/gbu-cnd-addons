import api from "../../services/api/index.js";

export default {
    getEvents (data) {
        return api().post('/addons/event/list', data);
    },
    assignEvents(data) {
        return api().post('/addons/event/assign', data);
    }
}
