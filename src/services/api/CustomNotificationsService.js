import axios from 'axios'

export default {
    getUserNotifications(userId, pageUrl) {
        return axios.create({
            // baseURL: 'https://nextmods.control-mos.ru/api',
            baseURL: 'https://cnd-dev.cr5.ru'
        }).get(`/addons/custom-events/user/${userId}?page_url=${pageUrl}`);
    },
}
