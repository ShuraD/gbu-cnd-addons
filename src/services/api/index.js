import axios from 'axios'

export default () => {
    return axios.create({
        // baseURL: 'https://cnd-dev.ddev.cnd'
        // baseURL: 'https://cnd-dev.cr5.ru'
        baseURL: 'https://cnd-api.control-mos.ru'
    })
}