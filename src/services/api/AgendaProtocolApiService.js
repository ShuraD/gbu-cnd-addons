import api from "../../services/api/index.js";

export default {
    formAgenda (dealId, data) {
        return api().post(`/addons/generate/agenda/${dealId}`, data);
    },
    formProtocol (dealId) {
        return api().post(`/addons/generate/protocol/${dealId}`);
    },
    fetchGlobalTags() {
        return api().post(`/addons/tags?global=true`);
    }
}
