import api from "../../services/api/index.js";

export default {
    createTask (data) {
        return api().post('/addons/task/paint', data);
    },
    getTaskById(taskId) {
        return api().get(`/megaplan/api/task/${taskId}`);
    }
}
