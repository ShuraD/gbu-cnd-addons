import api from "../../services/report_api/index.js";

export default {
    getList() {
        return api().get(`/new-api/employee`, {
            headers: {
                "x-cnd": 3948765589675867
            }
        });
    },
}
