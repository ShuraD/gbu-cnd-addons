import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: 'https://reports.control-mos.ru'
    })
}