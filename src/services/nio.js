import axios from 'axios'

export default {
    getByInn(inn, user) {
        return axios.create({
            baseURL: 'https://nextmods.control-mos.ru/api'
            // baseURL: 'https://cnd-dev.cr5.ru'
        }).post(`inn/get?value=${inn}`, user);
    },
    updateDeal(dealId, data) {
        return axios.create({
            baseURL: 'https://nextmods.control-mos.ru/api'
            // baseURL: 'https://cnd-dev.cr5.ru'
        }).post(`inn/post?deal_id=${dealId}`, data);
    }
}
