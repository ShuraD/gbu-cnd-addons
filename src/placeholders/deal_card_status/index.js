import '../../assets/scss/base.scss';
import Vue from 'vue';
import Form from '../../vue-components/assignEvents/Form.vue';
import DomManipulation from '../../DomManipulation';
import { actions } from '../../services/IntegrationModules/Registry';

'use strict';

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');

actions.register({
    name: 'Привязка событий',
    status: 'awaiting',
    trigger: 'bp 48',
});

if (pathnameParts.includes('bp') && pathnameParts.includes('58')) {
    actions.modify({
        name: 'Привязка событий',
        status: 'active',
        trigger: 'bp 48',
    });
    (async function ($) {
        if ($) {
            window.cnd = window.cnd || {};
            DomManipulation.addModalFormCnd('assign-event__modal-container-cnd');
            window.cnd.toggleAssignEventModal = function toggleModal() {
                let $modal = $('#assign-event__modal-container-cnd');
                if ($modal.is(':visible')) {
                    $modal.hide();
                } else {
                    $modal.css('display', 'flex');
                    // TODO: initiate event list loading
                }
            };

            let $button = await DomManipulation.find('button[data-name=addReminder]');
            let $container = $button.parent().parent();
            $container.find('> div:last').before('<button type="button" class="button-cnd primary" id="assign-events-cnd" onclick="window.cnd.toggleAssignEventModal()" style="margin-right: 20px">Привязать событие</button>');

            new Vue({
                el: '#assign-event__modal-container-cnd',
                render: h => h(Form)
            });
        } else {
            actions.modify({
                name: 'Привязка событий',
                status: 'error',
                reason: 'jQuery was not detected',
                trigger: 'bp 48',
            });
            console.error('jQuery was not detected');
        }
    })(jQuery);
}
