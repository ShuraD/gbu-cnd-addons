import { actions } from '../../services/IntegrationModules/Registry';

'use strict';

let $ = jQuery || null;
let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');

actions.register({ name: 'Модификатор карточки сделки БП 47', status: 'awaiting', trigger: 'bp 47' });

if ($ && pathnameParts.includes('bp') && pathnameParts.includes('47')) {
    actions.modify({ name: 'Модификатор карточки сделки БП 47', status: 'active', trigger: 'bp 47' });
    $('[data-tab=Task]').hide();
    $('[data-tab=Deal]').hide();
    $('button[title="+ Дело"]').hide();
    $('button[title="+ Напоминание"]').hide();
}