'use strict';

import Vue from 'vue';
const DomManipulation = require('../../DomManipulation');
const pp937Iframe = require('../../iframes/pp937');
import { actions } from '../../services/IntegrationModules/Registry';
import InnModalForm from '../../vue-components/inn/innModalForm.vue';

actions.register({ name: 'iframe "Отмена 937-ПП"', status: 'awaiting', trigger: 'bp 53' });
// actions.register({ name: 'iframe "Юристы"', status: 'awaiting', trigger: 'bp 35' });
actions.register({ name: 'iframe "Арендаторы"', status: 'awaiting', trigger: 'bp 59' });

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');
let programId;
// PP937
if (pathnameParts.includes('bp') && pathnameParts.includes('53')) {
    actions.modify({ name: 'iframe "Отмена 937-ПП"', status: 'active', trigger: 'bp 53' });
    DomManipulation.observe('button[data-name="addPositionButton"]', 'visibility', function (el, state) {
        state === 'visible' && pp937Iframe(el).then(() => {
            actions.modify({ name: 'iframe "Отмена 937-ПП"', status: 'awaiting', trigger: 'bp 53' });
        });
    });
} else {
    // TODO: check if url contains "deals" & "card"
    if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
        // TODO: find program id on the page
        const programLink = document.querySelector('a[href^="/logic/program/"]');
        const programLinkParts = programLink.href.split('/');
        const programIdPos = 5;
        programId = programLinkParts[programIdPos];
        if (parseInt(programId) === 53) {
            actions.modify({ name: 'iframe "Отмена 937-ПП"', status: 'active', trigger: 'bp 53' });
            DomManipulation.observe('button[data-name="addPositionButton"]', 'visibility', function (el, state) {
                state === 'visible' && pp937Iframe(el).then(() => {
                    actions.modify({ name: 'iframe "Отмена 937-ПП"', status: 'awaiting', trigger: 'bp 53' });
                });
            });
        }
    }
}

if (pathnameParts.includes('bp') && pathnameParts.includes('74')) {
    // Modal section
    DomManipulation.addModalFormCnd('inn__modal-container-cnd');
    new Vue({
        el: '#inn__modal-container-cnd',
        render: h => h(InnModalForm)
    });
} else {
    // TODO: ...
}

// if (pathnameParts.includes('bp') && pathnameParts.includes('35')) {
//     actions.register({ name: 'iframe "Юристы"', status: 'active', trigger: 'bp 35' });
//     DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
//         state === 'visible' && lawyersIframe(el).then(() => {
//             actions.modify({ name: 'iframe "Юристы"', status: 'awaiting', trigger: 'bp 35' });
//         });
//     });
// }

// if (pathnameParts.includes('bp') && pathnameParts.includes('59')) {
//     actions.register({ name: 'iframe "Арендаторы"', status: 'active', trigger: 'bp 59' });
//     DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
//         state === 'visible' && grantIframe(el).then(() => {
//             actions.modify({ name: 'iframe "Арендаторы"', status: 'awaiting', trigger: 'bp 59' });
//         });
//     });
// }
