'use strict';

const DomManipulation = require('../../DomManipulation');
const grantIframe = require('../../iframes/grant');
import { actions } from '../../services/IntegrationModules/Registry';

actions.register({ name: 'iframe "Арендаторы"', status: 'awaiting', trigger: 'bp 59' });

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');

if (pathnameParts.includes('bp') && pathnameParts.includes('59')) {
    actions.register({ name: 'iframe "Арендаторы"', status: 'active', trigger: 'bp 59' });
    DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
        state === 'visible' && grantIframe(el).then(() => {
            actions.modify({ name: 'iframe "Арендаторы"', status: 'awaiting', trigger: 'bp 59' });
        });
    });
}
