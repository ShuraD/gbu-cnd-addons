import '../../assets/scss/base.scss';
import Vue from 'vue';
import Form from "../../vue-components/createTask/Form_test.vue";
import DomManipulation from '../../DomManipulation';
import { actions } from '../../services/IntegrationModules/Registry';

actions.register({ name: 'Расписать', status: 'awaiting', trigger: 'task' });

(async function ($) {
    if ($) {
        actions.modify({ name: 'Расписать', status: 'active' });
        DomManipulation.addModalFormCnd('create-task__modal-container-cnd_test');

        let completeBtn = await DomManipulation.find('span:contains(Завершить)');
        completeBtn = $(completeBtn.get(3));
        let containerBtn = completeBtn
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .parent();

        let triggerBtn = $('<button id="paint-button-test" type="button" style="margin-right: 15px">Расписать</button>');
        triggerBtn.css({
            "margin-right": "15px",
            "background-color": "inherit",
            "border": "none",
            "text-transform": "uppercase",
            "color": "blueviolet",
            "font-family": "Roboto",
            "font-size": "12px",
            "font-weight": "bold",
            "outline": "none"
        });
        triggerBtn.hover(function () {
            $(this).css({
                "background-color": "rgba(138,43,226, .1)"
            })
        }, function () {
            $(this).css({
                "background-color": "inherit"
            })
        });
        triggerBtn.on('click', function () {
            let $modal = $('#create-task__modal-container-cnd_test');
            if ($modal.is(':visible')) {
                $modal.hide();
            } else {
                $modal.css('display', 'flex');
                // TODO: initiate event list loading
            }
        });
        containerBtn.before(triggerBtn);
        new Vue({
            el: '#create-task__modal-container-cnd_test',
            render: h => h(Form)
        });
    } else {
        actions.modify({ name: 'Расписать', status: 'error', reason: 'jQuery was not detected' });
        console.error('jQuery was not detected');
    }
})(jQuery);
