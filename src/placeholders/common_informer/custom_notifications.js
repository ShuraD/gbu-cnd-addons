import '../../assets/scss/base.scss';
import Vue from "vue";
import CustomNotificationsModalForm from "../../vue-components/customNotifications/ModalForm.vue";
import DomManipulation from '../../DomManipulation';

DomManipulation.addModalFormCnd('custom-notifications__modal-container-cnd');

new Vue({
    el: '#custom-notifications__modal-container-cnd',
    render: h => h(CustomNotificationsModalForm)
});
