import '../../assets/scss/base.scss';
import Vue from 'vue';
import Sticker from '../../vue-components/IntegrationModules/Sticker.vue';
import ExcelSticker from '../../vue-components/IntegrationModules/ExcelSticker.vue';
import RobotSticker from '../../vue-components/IntegrationModules/RobotSticker.vue';
import DomManipulation from '../../DomManipulation';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faDiceD20, faFileExcel, faCropAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Supervisor from '../../vue-components/IntegrationModules/Supervisor.vue';

library.add(faDiceD20);
library.add(faFileExcel);
library.add(faCropAlt);
Vue.component('font-awesome-icon', FontAwesomeIcon);
DomManipulation.addCustomSelector('integration-modules-app');
DomManipulation.addCustomSelector('excel__sticker');
DomManipulation.addCustomSelector('robot__sticker');
DomManipulation.addModalFormCnd('integration-modules__modal-container-cnd');

// new Vue({
//     el: '#integration-modules-app',
//     render: h => h(Sticker)
// });

new Vue({
    el: '#integration-modules__modal-container-cnd',
    render: h => h(Supervisor)
});

new Vue({
    el: '#excel__sticker',
    render: h => h(ExcelSticker)
});

new Vue({
    el: '#robot__sticker',
    render: h => h(RobotSticker)
});
