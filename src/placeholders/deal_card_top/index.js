import '../../assets/scss/base.scss';
import Vue from 'vue';

import DomManipulation from '../../DomManipulation';
import AgendaForm from "../../vue-components/agendaProtocol/AgendaForm.vue";
import ProtocolForm from "../../vue-components/agendaProtocol/ProtocolForm.vue";
import { actions } from '../../services/IntegrationModules/Registry';

actions.register({ name: 'Формирование повестки/протокола', status: 'awaiting', trigger: ['Сформировать повестку', 'Сформировать протокол'] });

(async function ($) {
    if ($) {
        window.cnd = window.cnd || {};
        DomManipulation.addModalFormCnd('form-agenda__modal-container-cnd');
        DomManipulation.addModalFormCnd('form-protocol__modal-container-cnd');
        window.cnd.toggleFormAgendaModal = function toggleModal() {
            let $modal = $('#form-agenda__modal-container-cnd');
            if ($modal.is(':visible')) {
                $modal.hide();
            } else {
                $modal.css('display', 'flex');
            }
        };
        window.cnd.toggleFormProtocolModal = function toggleModal() {
            let $modal = $('#form-protocol__modal-container-cnd');
            if ($modal.is(':visible')) {
                $modal.hide();
            } else {
                $modal.css('display', 'flex');
            }
        };

        let $agendaPlaceholder = await DomManipulation.find('div:contains(Сформировать_повестку)');
        let $protocolPlaceholder = await DomManipulation.find('div:contains(Сформировать_протокол)');
        actions.modify({ name: 'Формирование повестки/протокола', status: 'active', trigger: ['div:contains(Сформировать повестку)', 'div:contains(Сформировать протокол)'] });
        if (!$agendaPlaceholder.length) {
            console.error('Отсутствует плейсхолдер "Сформровать_повестку"');
        }
        if (!$protocolPlaceholder.length) {
            console.error('Отсутствует плейсхолдер "Сформровать_протокол"');
        }
        $agendaPlaceholder = $agendaPlaceholder.last();
        $protocolPlaceholder = $protocolPlaceholder.last();
        $agendaPlaceholder = $agendaPlaceholder.parent();
        $protocolPlaceholder = $protocolPlaceholder.parent();
        $agendaPlaceholder.replaceWith('<button type="button" class="button-cnd" id="form-agenda-cnd" onclick="window.cnd.toggleFormAgendaModal()" style="margin-left: 20px">Сформировать повестку</button>');
        $protocolPlaceholder.replaceWith('<button type="button" class="button-cnd" id="form-protocol-cnd" onclick="window.cnd.toggleFormProtocolModal()" style="margin-left: 20px">Сформировать протокол</button>');
        new Vue({
            el: '#form-agenda__modal-container-cnd',
            render: h => h(AgendaForm)
        });
        new Vue({
            el: '#form-protocol__modal-container-cnd',
            render: h => h(ProtocolForm)
        });
    } else {
        actions.modify({ name: 'Формирование повестки/протокола', status: 'error', trigger: ['div:contains(Сформировать повестку)', 'div:contains(Сформировать протокол)'] });
        console.error('jQuery was not detected');
    }
})(jQuery);