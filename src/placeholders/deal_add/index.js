'use strict';

import DomManipulation from '../../DomManipulation';
import EmployeeCreateProcessForm from '../../processes/employee_create_form';
import ApplicationForAbsenceProcessForm from '../../processes/application_for_absence_form';
import ApplicationForProvisionProcessForm from '../../processes/application_for_provision_form';
import TreatmentProcessForm from '../../processes/treatment_form';
import AxyProcessForm from '../../processes/axy_form';
import { actions } from '../../services/IntegrationModules/Registry';

const ABSENCE_FORM_MODIFIER = 'Модификатор формы "Создание заявки на отсутствие"';
const EMPLOYEE_FORM_MODIFIER = 'Модификатор формы "Создание новых сотрудников"';
const PROVISION_FORM_MODIFIER = 'Модификатор формы "Заявка по обеспечению 2.0"';
const TREATMENT_FORM_MODIFIER = 'Модификатор формы "Обращения 2.0"';
const AXY_FORM_MODIFIER = 'Модификатор формы "Поддержка АХУ"';

actions.register({ name: ABSENCE_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание заявки на отсутствие по схеме' });
actions.register({ name: EMPLOYEE_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание новых сотрудников по схеме' });
actions.register({ name: PROVISION_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание сделки по схеме Заявка по обеспечению 2.0' });
actions.register({ name: TREATMENT_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание обращения по схеме Обращения 2.0' });
actions.register({ name: AXY_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание заявки по схеме Поддержка АХУ' });

DomManipulation.observe('div:contains("Создание заявки на отсутствие по схеме")', 'visibility', function (el, state) {
    actions.modify({ name: ABSENCE_FORM_MODIFIER, status: 'active', trigger: 'Создание заявки на отсутствие по схеме' });
    state === 'visible' && ApplicationForAbsenceProcessForm(el).then(() => {
        actions.modify({ name: ABSENCE_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание заявки на отсутствие по схеме'});
    });
});
DomManipulation.observe('div:contains("Создание новых сотрудников по схеме")', 'visibility', function (el, state) {
    actions.modify({ name: EMPLOYEE_FORM_MODIFIER, status: 'active', trigger:  'Создание новых сотрудников по схеме'});
    state === 'visible' && EmployeeCreateProcessForm(el).then(() => {
        actions.modify({ name: EMPLOYEE_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание новых сотрудников по схеме' });
    });
});
// DomManipulation.observe('[data-autocomplete-options="1002108"]', 'visibility', function(el, state) {
//     state === 'visible' && hideListItems(el);
// });
DomManipulation.observe('div:contains("Создание сделки по схеме Заявка по обеспечению 2.0")', 'visibility', function (el, state) {
    actions.modify({ name: PROVISION_FORM_MODIFIER, status: 'active', trigger: 'Создание сделки по схеме Заявка по обеспечению 2.0'});
    state === 'visible' && ApplicationForProvisionProcessForm(el).then(() => {
        actions.modify({ name: PROVISION_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание сделки по схеме Заявка по обеспечению 2.0' });
    });
});
DomManipulation.observe('div:contains("Создание обращения по схеме Обращения 2.0")', 'visibility', function (el, state) {
    actions.modify({ name: TREATMENT_FORM_MODIFIER, status: 'active', trigger: 'Создание обращения по схеме Обращения 2.0' });
    state === 'visible' && TreatmentProcessForm(el).then(() => {
        actions.modify({ name: TREATMENT_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание обращения по схеме Обращения 2.0' });
    });
});
DomManipulation.observe('div:contains("Создание заявки по схеме Поддержка АХУ")', 'visibility', function (el, state) {
    actions.modify({ name: AXY_FORM_MODIFIER, status: 'active', trigger: 'Создание заявки по схеме Поддержка АХУ' });
    state === 'visible' && AxyProcessForm(el).then(() => {
        actions.modify({ name: AXY_FORM_MODIFIER, status: 'awaiting', trigger: 'Создание заявки по схеме Поддержка АХУ' });
    });
});
