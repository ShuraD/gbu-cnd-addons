const DomManipulation = require('../DomManipulation');

module.exports = function process(el) {
    return new Promise(async (resolve, reject) => {
        try {
            let $popup = el.parents('.modal');
            el.last().parent().append(DomManipulation.getStarLabel());
            await DomManipulation.findInputFieldByLabel('span:contains("Заявитель: ФИО заявителя")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Заявитель: Кабинет")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Заявитель: Телефон")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Руководитель заявителя")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Тип обработки")', $popup, 'input', true);
        } catch (e) {
            reject(e);
        }
    });
};
