const DomManipulation = require('../DomManipulation');
let $ = jQuery || null;

function convertMonth (value) {
    value = value.replace(/января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря/g, function (value) {
        switch (value) {
            case 'января':
                return 'january';
            case 'февраля':
                return 'february';
            case 'марта':
                return 'march';
            case 'апреля':
                return 'april';
            case 'мая':
                return 'may';
            case 'июня':
                return 'june';
            case 'июля':
                return 'july';
            case 'августа':
                return 'august';
            case 'сентября':
                return 'september';
            case 'октября':
                return 'october';
            case 'ноября':
                return 'november';
            case 'декабря':
                return 'december';
        }
    });

    value = value.slice(0, -3);
    value = value.replace(/([0-9]+)\s([a-z]+)\s/, '$2 $1 ');

    return value;
}

function isTimeValid (time) {
    let result;
    let [ hours, minutes ] = time.split(/\s|-|:|;|\.|,|\/|\\/g);
    if (hours.length !== 2) {
        return false;
    }
    if (minutes.length !== 2) {
        return false;
    }
    if (isNaN(Number.parseInt(hours[0]))) {
        return false;
    }
    if (isNaN(Number.parseInt(hours[1]))) {
        return false;
    }
    if (isNaN(Number.parseInt(minutes[0]))) {
        return false;
    }
    if (isNaN(Number.parseInt(minutes[1]))) {
        return false;
    }
    result = `${hours}:${minutes}`;
    hours = Number.parseInt(hours);
    minutes = Number.parseInt(minutes);
    if (!Number.isInteger(hours) || hours > 23) {
        return false;
    }
    if (!Number.isInteger(minutes) || minutes > 59) {
        return false;
    }

    return result;
}

module.exports = function process(el) {
    return new Promise(async (resolve, reject) => {
        try {
            let $programContainer = await DomManipulation.find('div:contains("Новый сотрудник")');
            let $popup = $programContainer.parents('[data-name=cmodal]');
            //$popup.find('> div').css('width', '1110px');
            let $planDateOutField = await DomManipulation.findInputFieldByLabel('span:contains("Планируемая дата выхода")', $popup);
            let $receptionRedinessDateField = await DomManipulation.findInputFieldByLabel('span:contains("Дата проверки готовности к приему")', $popup);
            let saveBtn = $('button[title="Создать Нового сотрудника"]');

            await DomManipulation.findInputFieldByLabel('span:contains("ФИО соискателя")', $programContainer, 'input', true, 'right');
            await DomManipulation.findInputFieldByLabel('span:contains("Организация, в которую выходит соискатель")', $programContainer, 'input', true, 'right');
            await DomManipulation.findInputFieldByLabel('span:contains("№ кабинета размещения (если ГБУ ЦНД)")', $programContainer, 'input', true, 'right');
            // await DomManipulation.findInputFieldByLabel('span:contains("Проектный офис")', $programContainer, 'input', false, 'right');
            // await DomManipulation.findInputFieldByLabel('span:contains("Отдел")', $programContainer, 'input', false, 'right');
            // await DomManipulation.findInputFieldByLabel('span:contains("Должность")', $programContainer, 'input', false, 'right');
            await DomManipulation.findInputFieldByLabel('span:contains("Планируемая дата выхода")', $programContainer, 'input', true, 'right');
            await DomManipulation.findInputFieldByLabel('span:contains("Дата проверки готовности к приему")', $programContainer, 'input', true, 'right');

            saveBtn.on('click', function () {
                let planDateOutValue = new Date(convertMonth($planDateOutField.val()));
                let receptionRedinessDateValue = new Date(convertMonth($receptionRedinessDateField.val()));
                // console.log('planDateOutValue', planDateOutValue);
                // console.log('receptionRedinessDateValue', receptionRedinessDateValue);
                if (planDateOutValue !== 'Invalid Date' && receptionRedinessDateValue !== 'Invalid Date') {
                    // console.log('planDateOutValue.getTime()', planDateOutValue.getTime());
                    // console.log('receptionRedinessDateValue.getTime()', receptionRedinessDateValue.getTime());
                    if (planDateOutValue.getTime() <= receptionRedinessDateValue.getTime()) {
                        alert('Дата проверки готовности к приему не может быть назначена позже или в один день с планируемой датой выхода');
                        return false;
                    }
                }
            });

            // let $projectOfficeField = await DomManipulation.findInputFieldByLabel('span:contains("Проектный офис")', $popup);
            // DomManipulation.observe($projectOfficeField, 'value', setActiveDeps);
        } catch (e) {
            reject(e);
        }
    })
};
