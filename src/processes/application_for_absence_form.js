const DomManipulation = require('../DomManipulation');
let $ = jQuery || null;

function convertMonth (value) {
    value = value.replace(/января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря/g, function (value) {
        switch (value) {
            case 'января':
                return 'january';
            case 'февраля':
                return 'february';
            case 'марта':
                return 'march';
            case 'апреля':
                return 'april';
            case 'мая':
                return 'may';
            case 'июня':
                return 'june';
            case 'июля':
                return 'july';
            case 'августа':
                return 'august';
            case 'сентября':
                return 'september';
            case 'октября':
                return 'october';
            case 'ноября':
                return 'november';
            case 'декабря':
                return 'december';
        }
    });

    value = value.slice(0, -3);
    value = value.replace(/([0-9]+)\s([a-z]+)\s/, '$2 $1 ');

    return value;
}

function isTimeValid (time) {
    let result;
    let [ hours, minutes ] = time.split(/\s|-|:|;|\.|,|\/|\\/g);
    if (hours.length !== 2) {
        return false;
    }
    if (minutes.length !== 2) {
        return false;
    }
    if (isNaN(Number.parseInt(hours[0]))) {
        return false;
    }
    if (isNaN(Number.parseInt(hours[1]))) {
        return false;
    }
    if (isNaN(Number.parseInt(minutes[0]))) {
        return false;
    }
    if (isNaN(Number.parseInt(minutes[1]))) {
        return false;
    }
    result = `${hours}:${minutes}`;
    hours = Number.parseInt(hours);
    minutes = Number.parseInt(minutes);
    if (!Number.isInteger(hours) || hours > 23) {
        return false;
    }
    if (!Number.isInteger(minutes) || minutes > 59) {
        return false;
    }

    return result;
}

module.exports = function process(el) {
    return new Promise(async (resolve, reject) => {
        try {
            let $programContainer = await DomManipulation.find('div:contains("Заявка на отсутствие")');
            let $popup = $programContainer.parents('.modal');
            let $typeOfAbsentInput = await DomManipulation.findInputFieldByLabel('span:contains("Тип")', $popup);

            let $star = $('<span style="color:red"> *</span>');
            $programContainer.parent().last().parent().append('<p style="display:block;color:red;margin: 15px 0 0 0;">* <small>Звездочкой отмечены обязательные поля</small></p>');

            let $wholeDay = await DomManipulation.find('span:contains("Целый день")', $popup);
            $wholeDay = $wholeDay.last();
            let $wholeDayParent = $wholeDay.parents('div').first();
            let $wholeDayInput = $wholeDayParent.find('input[type=checkbox]');
            let $wholeDayInputParent = $wholeDayInput.parent().parent().parent();
            $wholeDayInputParent.css('position', 'relative');

            DomManipulation.observe($typeOfAbsentInput, 'value', function (el) {
                if (typeof el.jquery === 'undefined') {
                    el = $(this);
                }
                let title = 'Для "Отпуск", "Отпуск за свой счет", "Больничный" галка всегда включена';
                switch (el.val()) {
                    case 'Отпуск':
                    case 'Отпуск за свой счет':
                    case 'Больничный':
                        if ($xlinkwholeDayInput.attr('xlink:href') === '#icon-CHECKBOX_BLANK-MEDIUM') {
                            $wholeDayInput.trigger('click');
                        }
                        $wholeDayInputParent.find('#custom-overlay').remove();
                        $wholeDayInputParent.append(`<div id="custom-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0" title='Для "Отпуск", "Отпуск за свой счет", "Больничный" галка всегда включена'></div>`);
                        let $customOverlay = $('#custom-overlay');
                        $customOverlay.on('click', function () {
                            alert(title);
                        });
                        break;
                    default:
                        $wholeDayInputParent.find('#custom-overlay').remove();
                }
            });

            let xlinkwholeDayInputParent = $wholeDayParent.get(0);
            let xlinkwholeDayInput = xlinkwholeDayInputParent.getElementsByTagName('use')[0];
            let $xlinkwholeDayInput = $(xlinkwholeDayInput);

            let $type = await DomManipulation.find('span:contains("Тип")', $popup);
            $type = $type.last();
            $type.append($star.clone());

            let $dateLeave = await DomManipulation.find('span:contains("Дата ухода")', $popup);
            $dateLeave = $dateLeave.last();
            $dateLeave.append($star.clone());
            let $dateLeaveParent = $dateLeave.parents('div').first();
            let $dateLeaveField = $dateLeaveParent.find('input');

            let $timeLeave = await DomManipulation.find('span:contains("Время ухода")', $popup);
            $timeLeave = $timeLeave.last();
            $timeLeave.append($star.clone());
            let $timeLeaveParent = $timeLeave.parents('div').first();
            let $timeLeaveField = $timeLeaveParent.find('textarea');

            let $dateComeIn = await DomManipulation.find('span:contains("Дата прихода")', $popup);
            $dateComeIn = $dateComeIn.last();
            $dateComeIn.append($star.clone());
            let $dateComeInParent = $dateComeIn.parents('div').first();
            let $dateComeInField = $dateComeInParent.find('input');

            let $timeComeIn = await DomManipulation.find('span:contains("Время прихода")', $popup);
            $timeComeIn = $timeComeIn.last();
            $timeComeIn.append($star.clone());
            let $timeComeInParent = $timeComeIn.parents('div').first();
            let $timeComeInField = $timeComeInParent.find('textarea');

            let $matchingLabel = await DomManipulation.find('label:contains("Согласующий")', $popup);
            $matchingLabel = $matchingLabel.first();
            $matchingLabel.append($star.clone());

            let $wholeDayInputLabel = $wholeDayInput.parents('label').first();
            $wholeDayInputLabel.on('click', function () {
                if ($xlinkwholeDayInput.attr('xlink:href') === '#icon-CHECKBOX-MEDIUM') {
                    $timeComeInField
                        .val($timeComeInField.data('prevVal'))
                        .text($timeComeInField.data('prevVal'))
                        .html($timeComeInField.data('prevVal'));
                    $timeLeaveField
                        .val($timeLeaveField.data('prevVal'))
                        .text($timeLeaveField.data('prevVal'))
                        .html($timeLeaveField.data('prevVal'));
                    $timeComeInField.attr('disabled', false);
                    $timeLeaveField.attr('disabled', false);
                }
                $timeComeInField.data('prevVal', $timeComeInField.html());
                $timeLeaveField.data('prevVal', $timeLeaveField.html());
                if ($xlinkwholeDayInput.attr('xlink:href') === '#icon-CHECKBOX_BLANK-MEDIUM') {
                    $timeComeInField.html('23:59').val('23:59').text('23:59').attr('disabled', 'disabled');
                    $timeLeaveField.html('00:00').val('00:00').text('00:00').attr('disabled', 'disabled');
                }
            });

            if ($xlinkwholeDayInput.attr('xlink:href') === '#icon-CHECKBOX-MEDIUM') {
                $timeComeInField.html('23:59').val('23:59').text('23:59').attr('disabled', 'disabled');
                $timeLeaveField.html('00:00').val('00:00').text('00:00').attr('disabled', 'disabled');
            }

            $timeComeInField.on('focusout', function () {
                let $this = $(this);
                let formattedTime = isTimeValid($this.html());
                if (!formattedTime) {
                    alert('Некорректный формат времени, ожидалось чч:мм');
                } else {
                    $this.html(formattedTime).val(formattedTime).text(formattedTime);
                }
            });

            $timeLeaveField.on('focusout', function () {
                let $this = $(this);
                let formattedTime = isTimeValid($this.html());
                if (!formattedTime) {
                    alert('Некорректный формат времени, ожидалось чч:мм');
                } else {
                    $this.html(formattedTime).val(formattedTime).text(formattedTime);
                }
            });

            $typeOfAbsentInput.on('valueChanged', function () {
                let $this = $(this);
                let title = 'Для "Отпуск", "Отпуск за свой счет", "Больничный" галка всегда включена';

                switch ($this.val()) {
                    case 'Отпуск':
                    case 'Отпуск за свой счет':
                    case 'Больничный':
                        if ($xlinkwholeDayInput.attr('xlink:href') === '#icon-CHECKBOX_BLANK-MEDIUM') {
                            $wholeDayInput.trigger('click');
                        }
                        $wholeDayInputParent.find('#custom-overlay').remove();
                        $wholeDayInputParent.append(`<div id="custom-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0" title='Для "Отпуск", "Отпуск за свой счет", "Больничный" галка всегда включена'></div>`);
                        let $customOverlay = $('#custom-overlay');
                        $customOverlay.on('click', function () {
                            alert(title);
                        });
                        break;
                    default:
                        $wholeDayInputParent.find('#custom-overlay').remove();
                }
            });

            let saveBtn = $('button[title="Создать Заявку на отсутствие"]');
            saveBtn.on('click', function () {
                if (!isTimeValid($timeLeaveField.val())) {
                    alert('Некорректный формат даты поля "Время прихода", ожидалось чч:мм');
                    return false;
                }
                if (!isTimeValid($timeLeaveField.val())) {
                    alert('Некорректный формат даты поля "Время ухода", ожидалось чч:мм');
                    return false;
                }
                let leaveDate = new Date(convertMonth($dateLeaveField.val()) + ' ' + $timeLeaveField.val());
                let comeInDate = new Date(convertMonth($dateComeInField.val()) + ' ' + $timeComeInField.val());
                if (leaveDate !== 'Invalid Date' && comeInDate !== 'Invalid Date') {
                    if (leaveDate.getTime() > comeInDate.getTime()) {
                        alert('Дата/Время прихода не должно быть раньше даты/времени ухода');
                        return false;
                    }
                }
            });
        } catch (e) {
            reject(e);
        }
    });
};
