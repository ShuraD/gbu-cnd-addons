const DomManipulation = require('../DomManipulation');

module.exports = function process(el) {
    return new Promise(async (resolve, reject) => {
        try {
            let $popup = el.parents('.modal');
            el.last().parent().append(DomManipulation.getStarLabel());
            await DomManipulation.findInputFieldByLabel('span:contains("Название")', $popup, 'input', true);
            // await DomManipulation.findInputFieldByLabel('span:contains("Заказчик: ФИО")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Заказчик: Контактный телефон")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Заказчик: Номер кабинета")', $popup, 'input', true);
        } catch (e) {
            reject(e);
        }
    });
};
