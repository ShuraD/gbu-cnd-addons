const DomManipulation = require('../DomManipulation');

module.exports = function process(el) {
    return new Promise(async (resolve, reject) => {
        try {
            let $popup = el.parents('.modal');
            el.last().parent().append(DomManipulation.getStarLabel());
            await DomManipulation.findInputFieldByLabel('span:contains("Название")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Суть")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Желаемая дата обработки обращения")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Почта для уведомлений")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Тип обращения")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Тип обработки обращения")', $popup, 'input', true);
            await DomManipulation.findInputFieldByLabel('span:contains("Заказчик: ФИО")', $popup, 'input', true);
        } catch (e) {
            reject(e);
        }
    });
};
