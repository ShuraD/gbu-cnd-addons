'use strict';

const DomManipulation = require('../DomManipulation');
const iframe = require('../iframes/tax_disputes_iframe_73_bp');
import { actions } from '../services/IntegrationModules/Registry';

actions.register({ name: 'Налоговые споры', status: 'awaiting', trigger: 'bp 73' });

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');

if (pathnameParts.includes('bp') && pathnameParts.includes('73')) {
    actions.modify({ name: 'Налоговые споры', status: 'active', trigger: 'bp 73' });
    DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
        state === 'visible' && iframe(el).then(() => {
            actions.modify({ name: 'Налоговые споры', status: 'awaiting', trigger: 'bp 73' });
        });
    });
} else {
    if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
        const programLink = document.querySelector('a[href^="/logic/program/"]');
        const programLinkParts = programLink.href.split('/');
        const programIdPos = 5;
        const programId = programLinkParts[programIdPos];
        if (parseInt(programId) === 73) {
            actions.modify({ name: 'Налоговые споры', status: 'active', trigger: 'bp 73' });
            DomManipulation.observe('button[data-name="addPositionButton"]', 'visibility', function (el, state) {
                state === 'visible' && iframe(el).then(() => {
                    actions.modify({ name: 'Налоговые споры', status: 'awaiting', trigger: 'bp 73' });
                });
            });
        }
    }
}

