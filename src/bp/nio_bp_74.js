'use strict';

const DomManipulation = require('../DomManipulation');
const iframe = require('../iframes/nio_iframe_74_bp');
import { actions } from '../services/IntegrationModules/Registry';

actions.register({ name: 'Юристы НИО', status: 'awaiting', trigger: 'bp 74' });

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');

if (pathnameParts.includes('bp') && pathnameParts.includes('74')) {
    actions.modify({ name: 'Юристы НИО', status: 'active', trigger: 'bp 74' });
    DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
        state === 'visible' && iframe(el).then(() => {
            actions.modify({ name: 'Юристы НИО', status: 'awaiting', trigger: 'bp 74' });
        });
    });
} else {
    if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
        const programLink = document.querySelector('a[href^="/logic/program/"]');
        const programLinkParts = programLink.href.split('/');
        const programIdPos = 5;
        const programId = programLinkParts[programIdPos];
        if (parseInt(programId) === 74) {
            actions.modify({ name: 'Юристы НИО', status: 'active', trigger: 'bp 74' });
            DomManipulation.observe('button[data-name="addPositionButton"]', 'visibility', function (el, state) {
                state === 'visible' && iframe(el).then(() => {
                    actions.modify({ name: 'Юристы НИО', status: 'awaiting', trigger: 'bp 74' });
                });
            });
        }
    }
}
