'use strict';

const DomManipulation = require('../DomManipulation');
const grantIframe = require('../iframes/grant_procuders');
import { actions } from '../services/IntegrationModules/Registry';

actions.register({ name: 'iframe "Гранты мониторинг ОИВ"', status: 'awaiting', trigger: 'bp 66' });

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');
let programId, dealId;

if (pathnameParts.includes('bp') && pathnameParts.includes('66')) {
    for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
        if (pathnameParts[i] === 'card') {
            dealId = pathnameParts[ i + 1 ];
            break;
        }
    }
    actions.modify({ name: 'iframe "Гранты мониторинг ОИВ"', status: 'active', trigger: 'bp 66' });
    DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
        state === 'visible' && grantIframe(el, dealId).then(() => {
            actions.modify({ name: 'iframe "Гранты мониторинг ОИВ"', status: 'awaiting', trigger: 'bp 66' });
        });
    });
} else {
    // TODO: check if url contains "deals" & "card"
    if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
        for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
            if (pathnameParts[i] === 'card') {
                dealId = pathnameParts[ i - 1 ];
                break;
            }
        }
        // TODO: find program id on the page
        const programLink = document.querySelector('a[href^="/logic/program/"]');
        console.log('programLink', programLink);
        console.log('programLink href', programLink.href);
        const programLinkParts = programLink.href.split('/');
        console.log('programLinkParts', programLinkParts);
        const programIdPos = 5;
        programId = programLinkParts[programIdPos];
        console.log('programId', programId);
        if (parseInt(programId) === 66) {
            actions.modify({ name: 'iframe "Гранты мониторинг ОИВ"', status: 'active', trigger: 'bp 66' });
            DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
                state === 'visible' && grantIframe(el, dealId).then(() => {
                    actions.modify({ name: 'iframe "Гранты мониторинг ОИВ"', status: 'awaiting', trigger: 'bp 66' });
                });
            });
        }
    }
}

