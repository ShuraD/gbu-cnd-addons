'use strict';

const DomManipulation = require('../DomManipulation');
const iframe = require('../iframes/citizen_appeals_iframe_86_bp');
import { actions } from '../services/IntegrationModules/Registry';

actions.register({ name: 'Обращения граждан', status: 'awaiting', trigger: 'bp 86' });

let pathname = window.location.pathname;
let pathnameParts = pathname.split('/');

if (pathnameParts.includes('bp') && pathnameParts.includes('86')) {
    actions.modify({ name: 'Обращения граждан', status: 'active', trigger: 'bp 86' });
    DomManipulation.observe('button[data-name="lockedIcon"]', 'visibility', function (el, state) {
        state === 'visible' && iframe(el).then(() => {
            actions.modify({ name: 'Обращения граждан', status: 'awaiting', trigger: 'bp 86' });
        });
    });
} else {
    if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
        const programLink = document.querySelector('a[href^="/logic/program/"]');
        const programLinkParts = programLink.href.split('/');
        const programIdPos = 5;
        const programId = programLinkParts[programIdPos];
        if (parseInt(programId) === 86) {
            actions.modify({ name: 'Обращения граждан', status: 'active', trigger: 'bp 86' });
            DomManipulation.observe('button[data-name="addPositionButton"]', 'visibility', function (el, state) {
                state === 'visible' && iframe(el).then(() => {
                    actions.modify({ name: 'Обращения граждан', status: 'awaiting', trigger: 'bp 86' });
                });
            });
        }
    }
}

