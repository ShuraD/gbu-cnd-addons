import Dom from './Dom';

class BpCondition {
    constructor(bpId) {
        this.bpId = bpId;
        this.data = {
            bpId: bpId
        }
    }
    check() {
        let pathname = window.location.pathname;
        let pathnameParts = pathname.split('/');
        // narrow case: bpId === dealId
        if (pathnameParts.includes('bp') && pathnameParts.includes(this.bpId.toString())) {
            return true;
        } else {
            if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
                const programLink = document.querySelector('a[href^="/logic/program/"]');
                const programLinkParts = programLink.href.split('/');
                const programIdPos = 5;
                const programId = programLinkParts[programIdPos];
                return parseInt(programId) === this.bpId;
            }
        }
        return false;
    }
}

class SelectorCondition {
    constructor(selector) {
        this.selector = selector;
        this.data = {
            element: null
        }
    }
    check() {
        const element = Dom.find(this.selector);
        if (element && element.length > 0) {
            this.data.element = element;
            return true;
        }
        return false;
    }
}

class IframeCondition {
    constructor(bpId) {
        this.bpId = bpId;
        this.selector = 'button[data-name="lockedIcon"]';
        this.data = {
            container: null,
        }
    }
    check() {
        const bpCondition = new BpCondition(this.bpId);
        const selectorCondition = new SelectorCondition(this.selector);

        if (bpCondition.check() && selectorCondition.check()) {
            const element = selectorCondition.data.element;
            if (element && element.length) {
                this.data.container = element.parent().parent().parent();
                return true;
            }
        }
        return false;
    }
}

export { BpCondition, IframeCondition, SelectorCondition }
