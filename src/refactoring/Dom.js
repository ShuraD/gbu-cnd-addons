const $ = jQuery || null;

class Dom {
    constructor($) {
        if (null === $) {
            // TODO: throw error
        }
        this.$ = $;
    }
    find(selector) {
        return this.$(selector);
    }
    addModal(modalId) {
        let $body = $('body');
        let $modalContainer = $body.find('#modal-container-cnd');
        if (!$modalContainer.length) {
            $body.append('<div id="modal-container-cnd"></div>');
            $modalContainer = $body.find('#modal-container-cnd');
        }
        $modalContainer.append(`<div id="${modalId}"></div>`);
    }
    addSticker(stickerId) {
        let $body = $('body');
        let $stickerContainer = $body.find('#sticker-container-cnd');
        if (!$stickerContainer.length) {
            $body.append('<div id="sticker-container-cnd"></div>');
            $stickerContainer = $body.find('#sticker-container-cnd');
        }
        $stickerContainer.append(`<div id=${stickerId}""></div>`)
    }
}

module.exports = new Dom($);
