import hashGen from 'hash-generator';

const BASE_URL = 'https://law-new.ddev.cnd'; // dev
// const BASE_URL = 'https://nextmods.control-mos.ru'; // prod
// const BASE_URL_OLD = "https://law-new.cnd"; // dev
const BASE_URL_OLD = "https://law.control-mos.ru"; // prod

export default class {
    constructor() {
        this.addons = {};
        // this.conditions = {};
        this.scanFrequency = 3000;
        this.busy = false;
        this.logging = true;
        this.user = null;
        this.url = null;
        this.lastPathname = window.location.pathname;
        this.options = {};
    }
    register(addon) {
        const hash = hashGen(10);
        this.addons[hash] = addon;
    }
    getUser() {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await a9n.user();
                this.user = {
                    id: user.id,
                    name: user.name
                };
                resolve(this.user);
            } catch (e) {
                reject(e);
            }
        });
    }
    getBpId() {
        let bpId = null;

        let pathnameParts = window.location.pathname.split('/');
        if (pathnameParts.includes('bp')) {
            for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                if (pathnameParts[i] === 'bp') {
                    bpId = pathnameParts[ i + 1 ];
                    break;
                }
            }
        } else {
            if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
                const programLink = document.querySelector('a[href^="/logic/program/"]');
                const programLinkParts = programLink.href.split('/');
                const programIdPos = 5;
                bpId = programLinkParts[programIdPos];
            }
        }

        return bpId;
    }
    getDealId() {
        let dealId = null;

        let pathnameParts = window.location.pathname.split('/');
        if (pathnameParts.includes('bp')) {
            for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                if (pathnameParts[i] === 'card') {
                    dealId = pathnameParts[ i + 1 ];
                    break;
                }
            }
        } else {
            if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
                for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                    if (pathnameParts[i] === 'card') {
                        dealId = pathnameParts[ i - 1 ];
                        break;
                    }
                }
            }
        }

        return dealId;
    }
    scan() {
       setInterval(this.browse.bind(this), this.scanFrequency);
    }
    browse() {
        // console.log('SCANNING...');
        // console.log('addons', this.addons);
        const currentPathname = window.location.pathname;
        if (currentPathname !== this.lastPathname) {
            // console.log('SCANNING... NEW URL PATH NAME DETECTED');
            for (let hash in this.addons) {
                this.addons[hash].status = false;
            }
            this.lastPathname = currentPathname;
        }
        if (this.busy) {
            // this.logging && console.log('SCANNING... CANCEL');
            return;
        }
        this.busy = true;
        const options = {
            baseUrl: BASE_URL,
            baseUrlOld: BASE_URL_OLD,
            dealId: this.getDealId(),
            bpId: this.getBpId(),
            user: this.user,
            userId: this.user.id,
            userName: this.user.name,
            url: window.location.href
        }
        // console.log('options', options);
        for (let hash in this.addons) {
            const addon = this.addons[hash];
            const conditions = addon.conditions;
            let proceed = false;
            var data = {};
            conditions.map(c => {
                // console.log('condition', c);
                proceed = c.check();
                data = Object.assign({}, data, c.data);
            });
            // console.log('proceed', proceed);
            if (proceed && addon.status === false) {
                // console.log('addon', addon);
                // console.log('data', data);
                const unpacked = [];
                for (let prop in data) {
                    unpacked.push(data[prop]);
                }
                // console.log('unpacked', unpacked);
                addon.run(options, ...unpacked);
            }
        }
        this.busy = false;
    }
}
