import Supervisor from './Supervisor';
import Iframe from './addons/iframe';
import Inn from './addons/inn';
// import Sticker from './addons/sticker';
import { BpCondition, SelectorCondition, IframeCondition } from './Condition';
// import ExcelStickerView from './components/sticker/ExcelSticker.vue';
import Ws from './addons/ws';

(async function() {
    const supervisor = new Supervisor;
    // user null???
    await supervisor.getUser();
    // 53
    const bp_53_id = 53;
    const iframe_for_bp_53 = new Iframe(
        [ new IframeCondition(bp_53_id) ],
        '{baseUrlOld}/#/deal/{dealId}/object/list?user_id={userId}'
    );
    supervisor.register(iframe_for_bp_53);
    // 73
    const bp_73_id = 73;
    const iframe_for_bp_73 = new Iframe(
        [ new IframeCondition(bp_73_id) ],
        '{baseUrl}/tax_disputes#/deal/{dealId}/objects?user_id={userId}&user_name={userName}&url={url}'
    );
    supervisor.register(iframe_for_bp_73);
    // 74
    const bp_74_id = 74;
    const iframe_for_bp_74 = new Iframe(
        [ new IframeCondition(bp_74_id) ],
        '{baseUrl}/nio#/deal/{dealId}/objects?user_id={userId}&user_name={userName}&url={url}'
    );
    supervisor.register(iframe_for_bp_74);
    // inn
    const inn_for_bp_74 = new Inn(
        [ new BpCondition(bp_74_id) ]
    );
    supervisor.register(inn_for_bp_74);
    // excel sticker
    // const excel_sticker = new Sticker(
    //     [],
    //     'excel__sticker-container-cnd',
    //     ExcelStickerView
    // );
    supervisor.scan();
})();

(async function() {
    const ws = new Ws;
    const $trigger = await ws.watch('Перенос в связанную сделку', 30000);
    $trigger.on('click', function(event) {
        console.log('I got it!');
    });
})();

// (async function() {
//     const ws = new WebSocket('wss://localhost:9898');
//     ws.onopen = function (event) {
//         ws.send('my sending data');
//     }
// })();
