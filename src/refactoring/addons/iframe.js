import Addon from '../Addon';
const $ = jQuery || null;

export default class Iframe extends Addon {
    constructor(conditions, urlTemplate) {
        super(conditions);
        this.urlTemplate = urlTemplate;
    }
    buildUrl(options) {
        let url = this.urlTemplate;
        for (let optionKey in options) {
            const optionValue = options[optionKey];
            url = url.replace('{' + optionKey + '}', optionValue);
        }
        return url;
    }
    run(options, container) {
        this.status = true;
        const iframeUrl = this.buildUrl(options);
        const divs = container.find('> div');
        container.find('> div').each(function() {
            $(this).css('visibility', 'hidden');
        });
        container.css('position', 'relative');
        container.css('display', 'block');
        container.css('padding', '0');
        container.css('min-height', '400px');
        container.css('background-color', '#fff');
        container.prepend(`
<input type="button" id="screen-mode" style="position:absolute;top:0;right:0;padding:4px 8px;z-index: 9999" value="Полноэкранный режим"/>
<iframe id="iframe-cnd" src="${iframeUrl}" style="position: absolute;width: 100%; min-height: 400px; border: none;z-index: 9998"></iframe>
`);
        const screenModeBtn = document.getElementById('screen-mode');
        screenModeBtn.addEventListener('click', changeScreenMode, false);

        function changeScreenMode() {
            const button = document.getElementById('screen-mode');
            const iframe = document.getElementById('iframe-cnd');
            iframe.classList.toggle('full-mode');
            if (iframe.classList.contains('full-mode')) {
                iframe.style.position = 'fixed';
                iframe.style.top = '0';
                iframe.style.left = '0';
                iframe.style.width = '100vw';
                iframe.style.height = '100vh';
                iframe.style.zIndex = '9999';
                iframe.style.backgroundColor = 'white';

                button.style.position = 'fixed';
                button.style.top = '0';
                button.style.right = '0';
                button.style.zIndex = '10000';
                button.value = 'Обычный режим';
            } else {
                iframe.style.position = 'absolute';
                iframe.style.width = '100%';
                iframe.style.height = 'initial';

                button.style.position = 'absolute';
                button.value = 'Полноэкранный режим';
            }
        }
    }
}
