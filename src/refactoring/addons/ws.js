const $ = jQuery || null;

export default class Ws {
    watch(scenario, waitUntil) {
        return new Promise((resolve, reject) => {
            try {
                if ($) {
                    let current = 0;
                    const interval = setInterval(function() {
                        if (current >= waitUntil) {
                            clearInterval(interval);
                            resolve(null);
                        }
                        const $link = $(`a:contains("${scenario}")`);
                        console.log('link', $link);
                        if ($link && $link.length) {
                            clearInterval(interval);
                            resolve($link);
                        }
                        current += 1000;
                    }, 1000);
                } else {
                    console.error('jQuery is null');
                    resolve(null);
                }
            } catch (e) {
                reject(e);
            }
        });
    }

}
