import Addon from '../Addon';
import Vue from 'vue';
import InnModal from '../components/inn/Modal.vue';
import Dom from '../Dom';

const INN_MODAL_CONTAINER_CND = 'inn__modal-container-cnd';

export default class Inn extends Addon {
    constructor(conditions) {
        super(conditions);
    }
    run(options) {
        this.status = true;
        const el =  Dom.find('#' + INN_MODAL_CONTAINER_CND);
        if (el.length === 0) {
            Dom.addModal(INN_MODAL_CONTAINER_CND);
            new Vue({
                el: '#' + INN_MODAL_CONTAINER_CND,
                render: h => h(InnModal, {
                    props: {
                        options,
                    },
                })
            });
        }
    }
}
