import Addon from '../Addon';
import Vue from 'vue';
import Dom from '../Dom';

export default class Sticker extends Addon {
    constructor(conditions, id, view) {
        super(conditions);
        this.id = id;
        this.view = view;
    }
    run(options) {
        this.status = true;
        Dom.addSticker(this.id);
        new Vue({
            el: '#' + this.id,
            render: h => h(this.view, {
                props: {
                    options,
                },
            })
        });
    }
}
