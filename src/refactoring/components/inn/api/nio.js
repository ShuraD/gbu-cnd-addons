import axios from 'axios'

export default {
    getByInn(inn, options) {
        return axios.create({
            // baseURL: 'https://nextmods.control-mos.ru/api'
            baseURL: 'https://cnd-dev.cr5.ru'
        }).post(`inn/get?value=${inn}`, options);
    },
    updateDeal(dealId, options) {
        return axios.create({
            // baseURL: 'https://nextmods.control-mos.ru/api'
            baseURL: 'https://cnd-dev.cr5.ru'
        }).post(`inn/post?deal_id=${dealId}`, options);
    }
}
