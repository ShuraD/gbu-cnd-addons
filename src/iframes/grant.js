module.exports = function insert(el) {
    return new Promise(async (resolve, reject) => {
        // const BASE_URL = "https://law-new.ddev.cnd";
        const BASE_URL = "https://law.control-mos.ru";
        let user = await a9n.user();
        let pathname = window.location.pathname;
        let pathnameParts = pathname.split('/');

        for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
            if (pathnameParts[i] === 'card') {
                dealId = pathnameParts[i+1];
                break;
            }
        }

        try {
            let container = el.parent().parent();
            container.css({ "padding": "12px 24px" });
            // let urlPath = `/#/deal/${dealId}/object/list`;
            let urlPath = `/grant/#/`;
            container.html(`<iframe id="iframe-cnd" scrolling="no" src="${BASE_URL}${urlPath}?user_id=${user.id}&deal_id=${dealId}" style="width: 100%; min-height: 500px; border: none"></iframe>`);
            resolve();
        } catch (e) {
            reject(e);
        }
    });
};
