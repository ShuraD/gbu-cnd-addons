module.exports = function insert(el) {
    return new Promise(async (resolve, reject) => {
        // const BASE_URL = "https://law-new.cnd";
        const BASE_URL = "https://law.control-mos.ru";
        let user = await a9n.user();
        let dealId;
        let pathname = window.location.pathname;
        let pathnameParts = pathname.split('/');

        if (pathnameParts.includes('bp') && pathnameParts.includes('53')) {
            for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                if (pathnameParts[i] === 'card') {
                    dealId = pathnameParts[ i + 1 ];
                    break;
                }
            }
        } else {
            if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
                for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                    if (pathnameParts[i] === 'card') {
                        dealId = pathnameParts[ i - 1 ];
                        break;
                    }
                }
            }
        }

        try {
            let container = el.parent().parent();
            container.css({ "padding": "12px 24px" });
            // let urlPath = `/#/deal/${dealId}/object/list`;
            let urlPath = `/#/deal/${dealId}/object/list`;
            container.html(`<iframe id="iframe-cnd" scrolling="no" src="${BASE_URL}${urlPath}?user_id=${user.id}" style="width: 100%; min-height: 500px; border: none"></iframe>`);
            resolve();
        } catch (e) {
            reject(e);
        }
    });
};
