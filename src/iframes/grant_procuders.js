module.exports = function insert(el, dealId) {
    return new Promise(async (resolve, reject) => {
        // const BASE_URL = "https://law-new.ddev.cnd";
        // const BASE_URL = "https://law.control-mos.ru";
        const BASE_URL = "https://reports.control-mos.ru";
        let user = await a9n.user();

        try {
            let container = el.parent().parent();
            container.css({ "padding": "12px 24px" });
            // let urlPath = `/#/deal/${dealId}/object/list`;
            let urlPath = `/grant_iframe#/deal/${dealId}/procedures/list`;
            container.css('padding', '0');
            container.html(`<iframe id="iframe-cnd-123" scrolling="no"  src="${BASE_URL}${urlPath}?user_id=${user.id}&deal_id=${dealId}" style="width: 100%; min-height: 400px; border: none;"></iframe>`);
            resolve();
        } catch (e) {
            reject(e);
        }
    });
};
