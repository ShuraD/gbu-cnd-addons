module.exports = function (el) {
    return new Promise(async (resolve, reject) => {
        // const BASE_URL = "https://law-new.ddev.cnd";
        const BASE_URL = "https://nextmods.control-mos.ru";
        let user = await a9n.user();
        let dealId;
        let pathname = window.location.pathname;
        let pathnameParts = pathname.split('/');

        if (pathnameParts.includes('bp') && pathnameParts.includes('73')) {
            for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                if (pathnameParts[i] === 'card') {
                    dealId = pathnameParts[ i + 1 ];
                    break;
                }
            }
        } else {
            if (pathnameParts.includes('deals') && pathnameParts.includes('card')) {
                for (let i = 0, counter = pathnameParts.length; i < counter; i++) {
                    if (pathnameParts[i] === 'card') {
                        dealId = pathnameParts[ i - 1 ];
                        break;
                    }
                }
            }
        }

        try {
            let container = el.parent().parent();
            container.css({ "padding": "12px 24px" });
            let urlPath = `/tax_disputes#/deal/${dealId}/objects`;
            container.css('padding', '0');
            container.html(`<input type="button" id="screen-mode" style="position:absolute;top:0;right:0;padding:4px 8px;" value="Полноэкранный режим"/><iframe id="iframe-cnd" src="${BASE_URL}${urlPath}?user_id=${user.id}&deal_id=${dealId}" style="width: 100%; min-height: 400px; border: none;"></iframe>`);
            resolve();
        } catch (e) {
            reject(e);
        }

        const screenModeBtn = document.getElementById('screen-mode');
        screenModeBtn.addEventListener('click', changeScreenMode, false);

        function changeScreenMode() {
            const button = document.getElementById('screen-mode');
            const iframe = document.getElementById('iframe-cnd');
            iframe.classList.toggle('full-mode');
            if (iframe.classList.contains('full-mode')) {
                iframe.style.position = 'fixed';
                iframe.style.top = '0';
                iframe.style.left = '0';
                iframe.style.width = '100vw';
                iframe.style.height = '100vh';
                iframe.style.zIndex = '9999';
                iframe.style.backgroundColor = 'white';

                button.style.position = 'fixed';
                button.style.top = '0';
                button.style.right = '0';
                button.style.zIndex = '10000';
                button.value = 'Обычный режим';
            } else {
                iframe.style.position = 'initial';
                iframe.style.width = '100%';
                iframe.style.height = 'initial';

                button.style.position = 'absolute';
                button.value = 'Полноэкранный режим';
            }
        }
    });
};
